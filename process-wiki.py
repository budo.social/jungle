#!/usr/bin/env python3

import re
import fugashi
import mwxml
import mwparserfromhell
from multiprocessing import current_process
import signal
import glob
from collections import defaultdict
import os
import sys

terms  = sys.argv[1].split(",") # ["名人", "達人", "博士", "玄人"]
files  = glob.glob('./data/raw/dumps.wikimedia.org/jawiki/latest/jawiki-latest-pages*.xml*.bz2')
tagger = fugashi.Tagger()

results_words = {}
for term in terms:
    results_words[term] = defaultdict(int)

results_categories = {}
for term in terms:
    results_categories[term] = defaultdict(int)

def save(filename, data):
    for term in terms:
        dirpath = "data/results/dumps.wikipedia.org/" + term 
        os.makedirs(dirpath, exist_ok=True)
        file = open(dirpath + "/" + filename + ".txt", 'w+')
        file.truncate()
        words = sorted(data[term].items(), key=lambda x: x[1], reverse = True)
        for word, count in words:
            file.write(word + "\t" + str(count) + "\n")
        file.close()

def on_quit():
    save('words', results_words)
    save('categories', results_categories)

def on_interrupt(signum, frame):
    on_quit()
    exit(1)

def process_dump(dump, path):
  for page in dump:
    for revision in page:
        yield page.id, page.title, revision.text

def parse():
    categories_regex = re.compile(r"\[\[[Cc]ategory:(.*)\]\]", re.MULTILINE)
    for page_id, title, text in mwxml.map(process_dump, files, threads=None):
        print("\r\033[KParsing page ", title, "(id:", page_id, ")", end = '', flush = True)
        title_tokens = [word.surface for word in tagger(title)]
        markup = mwparserfromhell.parse(text)
        text_tokens = [word.surface for word in tagger(markup.strip_code())]
        tokens = title_tokens + text_tokens
        
        for term in terms:
            if term in tokens:
                print("\r\033[KFound [" + term + "]:", title, flush = True)
                
                # Count/increment word occurences
                for token in tokens:
                    codepoint = ord(token[0])
                    if len(token) >= 2 and codepoint >= 0x4e00 and codepoint <= 0x9fbf:
                        results_words[term][token] += 1
                
                # Count increment categories occurences
                categories = re.findall(categories_regex, text)

                for category in categories:
                    results_categories[term][category] += 1
                    

if __name__ == '__main__':
    signal.signal(signal.SIGINT, on_interrupt)
    parse()
    on_quit()