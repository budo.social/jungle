#!/usr/bin/env python3

###
# Note: We should consider using the OpenArchive format for data harvesting
# should we ever wish to filter metadata locally (instead of relying of remote searches)
#
# https://www.openarchives.org/pmh/tools/
# https://iss.ndl.go.jp/api/oaipmh
#
# See also:
#
# https://iss.ndl.go.jp/information/api/riyou/
###
import sys
import os
from urllib.parse import quote
from urllib.request import URLopener
import xml.etree.ElementTree as ET

term = sys.argv[1]
filepath = "data/raw/ndlsearch/" + term
os.makedirs(filepath, exist_ok=True)

done = False
count = 0
total = 0
while not done:
    print("\r\033[K[" +  term + ":" + str(total) + "] Downloading page", str(count), end = '', flush = True)

    ##
    # https://iss.ndl.go.jp/api/opensearch?any=test&cnt=500&idx=1
    #
    # Todo: You cannot get more than 500 search results at the moment. Pagination seems to be broken
    ##
    searchurl = "https://iss.ndl.go.jp/api/opensearch?any="
    searchurl += quote(term.encode("utf-8"))
    searchurl += "&cnt=500&idx=" + str(count * 500 + 1)
    destination = filepath + "/" + str(count) + ".xml"
    
    # Download results
    file = URLopener()
    file.retrieve(searchurl, destination)

    # Check XML file
    tree = ET.parse(destination)
    total = int(tree.find("./channel/{http://a9.com/-/spec/opensearchrss/1.0/}totalResults").text)
    if count >= total:
        done = True
    else:
        count += 1