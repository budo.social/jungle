![Japanese Unique Next-Generation Language Engines (JUNGLE)](./logo.png)

**Created my free logo at LogoMakr.com**

This repositories hosts a series of scripts and tools to run linguistic breakdowns against well-known
sources of historical information about Japan (JPSearch, Wikipedia).

It will help you:

- Download the files locally for mining
- Write and customize scripts for word frequency, detected categorizations, etc

The tools are still quite rough to use. Any and all contributions would be most welcome.

## Todo

- [ ] Add proper support for the NDL search engine
- [ ] Fetch full text from books, magazines, etc when available

## Requirements

- Linux environment (WSL is fine if you are a Windows user)
- Python 3
- Virtualenv

## Setup

> Commands to run from within this project's folder

```shell
python3 -m venv --copies ./venv
source ./venv/bin/activate
pip3 install -r requirements.txt
```

## Usage

> Downloading the latest page data from Wikipedia

```shell
./fetch-wikipedia.sh
```

> Downloading search metadata for a search query on JPSearch

```shell
./fetch-jpsearch.py [word you wish to download results for]
```

> Downloading search metadata for a search query on NDL

```shell
./fetch-ndlsearch.py [word you wish to download results for]
```

> Process local data

```shell
./process-jpsearch.py
./process-wikipedia.py [comma-separated list of terms of interest]
```

> Generate graphs of processed data

```shell
./graph.py [sitename - see data/results] [type of data] [output destination] [smallest,biggest] [comma-separated terms] [word filters]
# Examples
./graph.py jpsearch words data/output.jpg 10,1000000 名人,達人 武,剣,柔,捕,合気,弓,居合,士,捕手,和術,将棋,茶,花,幕
./graph.py dumps.wikipedia.org words data/output.jpg 100,1000000 博士,名人,達人 武,剣,柔,捕,合気,弓,居合,士
```

> Example output

![example output](output.jpg)

You can generate image graphs (jpg, png, etc) or HTML ones (in which case they will be interactive).