#!/usr/bin/env python3

from urllib.parse import quote
from urllib.request import URLopener
import sys
import os

if len(sys.argv) == 1:
    print("Usage: " + sys.argv[0] + " [search term]")
    exit(1)

term = sys.argv[1]
filepath = "data/raw/jpsearch/" + term
os.makedirs(filepath, exist_ok=True)

search = "https://jpsearch.go.jp/api/item/search-so/jps-cross?csid=jps-cross&size=1000000&sort=sindex.sortTemporal%3Aasc&agg-cs=true"
search += "keyword=" + quote(term.encode("utf-8"))
destination = filepath + "/0.json"
file = URLopener()
file.retrieve(search, destination)