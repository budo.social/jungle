#!/usr/bin/env python3

import json
import fugashi
from collections import defaultdict
import os
import glob

termspath = glob.glob("data/raw/jpsearch/*/")
terms  = list(map(lambda val: val.rsplit("/")[-2], termspath))
tagger = fugashi.Tagger()
results = {}
for term in terms:
    results[term] = defaultdict(int)

def process_dump(dump, path):
  for page in dump:
    for revision in page:
        yield page.id, page.title, revision.text

for term in terms:
    data = json.load(open("data/raw/jpsearch/" + term + "/0.json"))
    
    for entry in data["list"]:
        common = entry["common"]
        title = common["title"]
        title = (title[:25] + "...") if len(title) > 25 else title
        print("\r\033[K[" +  term + "] Parsing page ", title, "(id:", entry["id"], ")", end = '', flush = True)
        tokens = [word.surface for word in tagger(common["title"])]

        if "description" in common:
            tokens += [word.surface for word in tagger(common["description"])]

        words = results[term]
        for token in tokens:
            codepoint = ord(token[0])
            if len(token) >= 2 and codepoint >= 0x4e00 and codepoint <= 0x9fbf:
                words[token] += 1 

    dirpath = "data/results/jpsearch/" + term
    os.makedirs(dirpath, exist_ok=True)
    file = open(dirpath + "/words.txt", 'w+')
    file.truncate()

    words = sorted(results[term].items(), key=lambda x: x[1], reverse = True)
    for word, count in words:
        file.write(word + "\t" + str(count) + "\n")
    file.close()