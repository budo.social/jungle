#!/usr/bin/env python3

import plotly.express as px
import pandas as pd
import glob
import sys
import regex

if len(sys.argv) < 4:
    print("Usage: " + sys.argv[0], "[site] [counttype] [output destination] [terms to include (optional)] [data type to include]")
    exit(1)

# https://coolors.co/1e1e24-affc41-f63cb8-00ffee-ff130f-ba8221
colors = (
    "#affc41", # Green Lizard
    "#F63CB8", # Shocking Pink
    "#00FFEE", # Turquoise Blue
    "#FF120F", # Red RYB
    "#ba8221", # Dark Goldenrod
)

site = sys.argv[1]
datatype = sys.argv[2]
output = sys.argv[3]

limits = (0,1000000)
if len(sys.argv) >= 5:
    limits = sys.argv[4].split(",")
    limits = [int(limit) for limit in limits]

includedterms = None
if len(sys.argv) >= 6:
    includedterms = sys.argv[5].split(",")

filters = None
filters_regexp = None
if len(sys.argv) >= 7:
    filters = sys.argv[6].split(",")
    filters_regexp = regex.compile(r"\L<words>", words=filters)

# List terms
sitepath = "data/results/" + site
termspath = glob.glob(sitepath + "/*/")
terms = []
data = None

for termpath in termspath:
    # Basic information
    filepath = termpath + datatype + ".txt"
    term = termpath.rsplit("/")[-2]
    
    # Exclude unwanted terms
    if includedterms is not None and term not in includedterms:
        continue

    # Load
    terms += (term,)
    raw_data = pd.read_csv(filepath, sep = '\t', names = [datatype, term])

    # Filter
    filtered_data = raw_data[(raw_data[term] >= limits[0]) & (raw_data[term] <= limits[1])]
    if filters is not None:
        filtered_data = filtered_data[
            filtered_data[datatype]
                .apply(
                    lambda word:
                        filters_regexp.search(word) is not None
                )
        ]

    # Join
    if data is None:
        data = filtered_data
    else:
        data = pd.merge_ordered(data, filtered_data, on = datatype)

# Generate the graph
graph_title = site + ": " + datatype + " occurences per selected term"
if filters is not None:
    graph_title += " (word filters: " + ",".join(filters) + ")"

fig = px.bar(
    data,
    width=3200, height=1500,
    x = datatype, 
    y = terms,
    title = graph_title, 
    template = "plotly_dark",
    text_auto='.2s',
    color_discrete_sequence = colors
)

# Update the graph's formatting
fig.update_layout(
    yaxis_title = 'occurences',
    showlegend = True,
    plot_bgcolor = "#1e1e24",
    paper_bgcolor = "#0f0f0f",
    legend = dict(
        title = "Terms",
        bgcolor='rgba(255, 255, 255, 0.1)',
        bordercolor='rgba(255, 255, 255, 0.5)'
    )
)

fig.update_traces(textposition='outside')
fig.update_xaxes(tickangle = -60)
# fig.update_yaxes(type = "log")

# Output HTML or an image, depending on the file extension provided
if output.endswith(".html") :
    fig.write_html(output)
else:
    fig.write_image(output)