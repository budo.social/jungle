#!/usr/bin/env bash

cd ./data/raw
wget \
    -np \
    -r \
    --accept-regex 'https:\/\/dumps\.wikimedia\.org\/jawiki\/latest\/jawiki-latest-pages-articles[0-9]+\..*' \
    https://dumps.wikimedia.org/jawiki/latest/
